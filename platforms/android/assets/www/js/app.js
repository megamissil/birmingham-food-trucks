// 'bft.services' is found in services.js
// 'bft.controllers' is found in controllers.js

angular.module('bft', ['ionic', 'ionic-material', 'firebase', 'ngCordova', 'bft.controllers', 'bft.services'])

.run(function ($ionicPlatform, $ionicLoading, $rootScope, $state, $q, $cordovaGeolocation, $ionicPopup, Markers) {
 // navigator.splashscreen.hide();

   $ionicLoading.show({
      template: "<img src='https://storage.googleapis.com/bham-food-trucks.appspot.com/BFT-loading-pig-3.1.gif' alt='loading gif'>",
      noBackdrop: false
   });
   
   $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
         cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
         // org.apache.cordova.statusbar required
         StatusBar.styleDefault();
      }

      // Hide the loader
      setTimeout(function() {
         $ionicLoading.hide();
      }, 3000);

      // Initialize global variables
      $rootScope.users        = firebase.database().ref('users'),
      $rootScope.markers      = {},
      $rootScope.currentUser  = firebase.auth().currentUser,
      $rootScope.userPromptedToCheckIn = false;

      // checkIn() and checkOut() are specific to authenticated vendors 
      $rootScope.checkIn = function () {
         var deferred = $q.defer();

         $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: true}).then(function (location) {
            var lat = location.coords.latitude,
                lon = location.coords.longitude;

            $rootScope.users.once('value').then(function (snapshot) {
              var trucks = snapshot.val();

              for (var key in trucks) {
                var value = trucks[key];

                if ($rootScope.currentUser && (value.email == $rootScope.currentUser.email)) {                  
                  firebase.database().ref('users/' + key).once('value').then(function (data) {
                    data.ref.update({latitude: lat, longitude: lon}).then(function (success) {                      
                      Markers.add(key, value);
                    }, function (error) {
                      deferred.reject({success: false, error: error});
                    });
                  }, function (error) {
                    deferred.reject({success: false, error: error});
                  });
                
                  break;
                }
              }
            }, function (error) {
              deferred.reject({success: false, error: error});
            });
         }, function (error) {
            deferred.reject({success: false, error: error});
         });

         deferred.resolve({success: true, error: false});

         return deferred.promise;
      };

      $rootScope.checkOut = function () {
         return $rootScope.users.once('value').then(function (snapshot) {
            var trucks = snapshot.val();

            for (var key in trucks) {
               var value = trucks[key];

               if ($rootScope.currentUser && (value.email == $rootScope.currentUser.email)) {
                  firebase.database().ref('users/' + key).once('value').then(function (data) {
                     return data.ref.update({latitude: '', longitude: ''});
                  });
               }
            }
         });
      };

      $rootScope.signOut = function () {
         $rootScope.checkOut().then(function (success) {
            // Remove the active user
            $rootScope.currentUser = null;

            // Sign out of Firebase
            firebase.auth().signOut();

            // Back to the landing page
            $state.go('landing');
         }, function (error) {
            console.warn(error);
         });
      };

      $rootScope.togglePrompt = function () {
         if ($rootScope.userPromptedToCheckIn === true) {
            $rootScope.userPromptedToCheckIn = false;
         }

         $state.go('landing');
      };

     // Attach a listener to the child_changed event on the users database
     $rootScope.users.on('child_changed', function (data) {
       if (!$rootScope.map) {
         return;
       }

       var lat = data.child('latitude').val(),
           lon = data.child('longitude').val();

        var position = new google.maps.LatLng(lat, lon);

       if (lat.length === 0 || lon.length === 0) {
         Markers.remove(data.key);

         return;
       } else {
         if (data.key in $rootScope.markers) {
           if ($rootScope.markers[data.key]['marker'].getPosition().equals(position) === false) {
             Markers.remove(data.key);
           } else {
             return;
           }
         }

         Markers.add(data.key, data.val());
       }
     });
   });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
   // Set up the various states which the app can be in.
   // Each state's controller can be found in controllers.js
   $ionicConfigProvider.tabs.position('bottom');
   $ionicConfigProvider.backButton.icon('ion-chevron-left');
   $ionicConfigProvider.scrolling.jsScrolling(false);


   $stateProvider

   .state('landing', {
      url: '/landing',
     templateUrl: 'templates/landing.html',
      controller: 'AppCtrl'

   })

   // setup menu
   .state('app', {
      cache: true,
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
   })

   .state('app.map', {
      cache: true,
      url: '/map',
      views: {
         'tab-map': {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
         }
      }
   })

   .state('app.trucks', {
      url: '/trucks',
      views: {
         'tab-trucks': {
            templateUrl: 'templates/trucks.html',
            controller: 'TrucksCtrl'
         }
      }
   })

   .state('app.favorites', {
      url: '/favorites',
      cache: false,
      views: {
         'tab-favorites': {
            templateUrl: 'templates/favorites.html',
            controller: 'FavoritesCtrl'
         }
      }
   })

   .state('about-us', {
      url: '/about-us',
      templateUrl: 'templates/about-us.html'
   })

   .state('contact', {
      url: '/contact',
      templateUrl: 'templates/contact.html'
   })

   .state('truck-login', {
      url: '/truck-login',
      templateUrl: 'templates/truck-login.html',
      controller: 'LoginCtrl'
   })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('landing');
});