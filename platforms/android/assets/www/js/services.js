"use strict";

angular.module('bft.services', ['ionic', 'firebase'])

.factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    return $firebaseAuth();
  }
])

.factory("Trucks", ["$firebaseArray", "$firebaseObject",
  function($firebaseArray, $firebaseObject) {
    return {
      all : function(type) {
        type = typeof type === 'undefined' ? 'array' : type;

        var trucks = firebase.database().ref("users");

        switch (type) {
          case 'array':
            return $firebaseArray(trucks);
          break;

          case 'object':
            return $firebaseObject(trucks);
          break;

          default:
            return trucks;
          break;
        }
      }
    };
  }
])

.factory("Markers", ['$rootScope', function ($rootScope) {
  var _icons = {
    truck: {
      url: 'https://storage.googleapis.com/bham-food-trucks.appspot.com/Markers/BFT-pinB64-orange.png',
      size: new google.maps.Size(40, 64),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0,64)
    },

    user: {
      url: 'https://storage.googleapis.com/bham-food-trucks.appspot.com/Markers/BFT-pinB64-blue.png',
      size: new google.maps.Size(40, 64),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0,64)
    }
  };

  var _addMarker = function (id, data) {
    if (id in $rootScope.markers) {
      return;
    }

    $rootScope.markers[id] = {};

    // Create an instance of google.maps.LatLng
    $rootScope.markers[id]['position'] = new google.maps.LatLng(data.latitude, data.longitude);

    // Create an instance of google.maps.Marker
    $rootScope.markers[id]['marker'] = new google.maps.Marker({
      map:       $rootScope.map,
      animation: google.maps.Animation.DROP,
      icon:      _icons.truck,
      title:     data.name,
      position:  $rootScope.markers[id]['position']
    });

    // Bind an instance of google.maps.InfoWindow to the new marker
    var output = 
      '<div id="info-content"><div id="siteNotice"></div>' +
      '<h4>' + data.name + '</h4>' +
      '<p>' + data.cuisine + '</p>' +
      '</div>';

    $rootScope.markers[id]['window'] = new google.maps.InfoWindow({
      content:     output,
      pixelOffset: new google.maps.Size(0, -30),
      position:    $rootScope.markers[id]['position']
    });

    // Listen for click events on the marker to toggle the new instance of InfoWindow
    $rootScope.markers[id]['event'] = $rootScope.markers[id]['marker'].addListener('click', function () {      
      $rootScope.markers[id]['window'].open($rootScope.map, $rootScope.markers[id]['marker']);
    });
  };

  var _removeMarker = function (id) {
    if (!id in $rootScope.markers) {
      return;
    }

    $rootScope.markers[id]['marker'].setMap(null);
    $rootScope.markers[id]['event'].remove();

    delete $rootScope.markers[id];
  };

  return {
    add:    _addMarker,
    remove: _removeMarker,
    icons:  _icons
  };
}]) 

.factory("Favorites", [ function () {
  var _get = function () {
    var favorites = localStorage.getItem('favorites');

    if ( !favorites ) {
      return [];
    } else {
      return JSON.parse(favorites);
    }
  };

  var favorites = _get();

  var _has = function (id) {
    if ( !id ) {
      return;
    }

    for (var truck in favorites) {      
      if (favorites[truck]['$id'] == id) {
        return truck;
      }
    }

    return false;
  };

  return {
    get: _get,
    has: _has,
    
    add: function (id) {
      if ( !id ) {
        return;
      }

      if ( _has(id) !== false ) {
        return;
      }

      favorites.push(id);

      localStorage.setItem('favorites', JSON.stringify(favorites));
    },

    remove: function (id) {
      if ( !id ) {
        return;
      }

      var index = _has(id);

      if ( !index ) {    
        return;
      }

      favorites.splice(index, 1);

      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
  };
}]);
