// 'bft.services' is found in services.js
// 'bft.controllers' is found in controllers.js

angular.module('bft', ['ionic', 'ionic-material', 'firebase', 'ngCordova', 'bft.controllers', 'bft.services'])

.run(function($ionicPlatform, $ionicLoading) {
   $ionicLoading.show({
      template: "<img src='https://storage.googleapis.com/bham-food-trucks.appspot.com/BFT-loading-pig-3.1.gif' alt='loading gif'>",
      noBackdrop: false
   });
   $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
         cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
         // org.apache.cordova.statusbar required
         StatusBar.styleDefault();
      }

      setTimeout(function() {
         $ionicLoading.hide();
      }, 3000);
   });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
   // Set up the various states which the app can be in.
   // Each state's controller can be found in controllers.js
   $ionicConfigProvider.tabs.position('bottom');
   $ionicConfigProvider.backButton.icon('ion-chevron-left');
   $ionicConfigProvider.scrolling.jsScrolling(false);


   $stateProvider

   .state('landing', {
      url: '/landing',
     templateUrl: 'templates/landing.html',
      controller: 'AppCtrl'

   })

   // setup menu
   .state('app', {
      cache: true,
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
   })

   .state('app.map', {
      cache: false,
      url: '/map',
      views: {
         'tab-map': {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
         }
      }
   })

   .state('app.trucks', {
      url: '/trucks',
      views: {
         'tab-trucks': {
            templateUrl: 'templates/trucks.html',
            controller: 'TrucksCtrl'
         }
      }
   })

   .state('app.favorites', {
      url: '/favorites',
      cache: false,
      views: {
         'tab-favorites': {
            templateUrl: 'templates/favorites.html',
            controller: 'FavoritesCtrl'
         }
      }
   })

   .state('about-us', {
      url: '/about-us',
      templateUrl: 'templates/about-us.html'
   })

   .state('contact', {
      url: '/contact',
      templateUrl: 'templates/contact.html'
   })

   .state('truck-login', {
      url: '/truck-login',
      templateUrl: 'templates/truck-login.html',
      controller: 'LoginCtrl'
   })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('landing');
});