"use strict";

angular.module('bft.controllers', ['ionic', 'firebase', 'bft.services'])

  .controller('AppCtrl', function($scope, $cordovaInAppBrowser) {

    var deviceInformation = ionic.Platform.device();

    var isWebView = ionic.Platform.isWebView();
    var isIPad = ionic.Platform.isIPad();
    var isIOS = ionic.Platform.isIOS();
    var isAndroid = ionic.Platform.isAndroid();
    var isWindowsPhone = ionic.Platform.isWindowsPhone();

    var currentPlatform = ionic.Platform.platform();
    var currentPlatformVersion = ionic.Platform.version();

  })

     .controller("LoginCtrl", function ($scope, $state, $ionicLoading, $ionicPopup) {
      
      $scope.logIn = function (user) {
         if (user && user.email && user.password) {
            firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(
              function () {
                $state.go('app.map');
              }, function (error) {
                $ionicLoading.hide();

                user.email = user.password = '';

                $ionicPopup.alert({
                  template: error.message,
                  title: 'LOGIN FAILED',
                  buttons: [{
                    type: 'button-assertive',
                    text: '<b>Ok</b>'
                  }]
                });

                $state.go('truck-login');
              }
            );
           } else {
             $ionicLoading.hide();
             $ionicPopup.alert({
               template: 'Please Check Credentials.',
               title: 'LOGIN FAILED',
               buttons: [{
                 type: 'button-assertive',
                 text: '<b>Ok<b>'
               }]
             });
          }
      };
   })


   .controller('LogoutCtrl', function($scope) {
     $scope.logOut = function(){
       firebase.auth().signOut();
     };
   })

     .controller('MapCtrl', function($rootScope, $scope, $state, $ionicPlatform, $ionicPopup, $cordovaGeolocation) {
        // This will initially place a user on the map

        var truckMarker = {
          url: 'https://storage.googleapis.com/bham-food-trucks.appspot.com/Markers/BFT-pin2-orange.png',
          size: new google.maps.Size(21, 32),
          origin: new google.maps.Point(0, 0)
        //  anchor: new google.maps.Point(0, 32) 
        };

        var userMarker = {
          url: 'https://storage.googleapis.com/bham-food-trucks.appspot.com/Markers/BFT-pin2-blue.png',
          size: new google.maps.Size(21, 32),
          origin: new google.maps.Point(0, 0)
        //  anchor: new google.maps.Point(0, 32) 
        };


        $scope.$on('$ionicView.loaded', function (event, data) {
           $scope.userPromptedToCheckIn = false;

           $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: true}).then(function (location) {
             var initialPosition = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);

             var mapOptions = {
                center: initialPosition,
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [
                       {
                           "featureType": "administrative",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative",
                           "elementType": "labels.text.fill",
                           "stylers": [
                               {
                                   "color": "#e24307"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative.locality",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative.locality",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "off"
                               },
                               {
                                   "color": "#f2f2ef"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.man_made",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "color": "#f2f2ef"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.natural.landcover",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.natural.terrain",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "visibility": "off"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "geometry",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               },
                               {
                                   "saturation": "-37"
                               },
                               {
                                   "lightness": "45"
                               },
                               {
                                   "gamma": "1.78"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "poi.park",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "gamma": "1.01"
                               },
                               {
                                   "saturation": "11"
                               },
                               {
                                   "hue": "#00ff7a"
                               },
                               {
                                   "lightness": "-4"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "-100"
                               },
                               {
                                   "lightness": "25"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "-1"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "color": "#7EBBD6"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "geometry.stroke",
                           "stylers": [
                               {
                                   "color": "#65A2BD"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "1"
                               },
                               {
                                   "weight": "1.00"
                               },
                               {
                                   "lightness": "1"
                               },
                               {
                                   "gamma": "1.01"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "saturation": "3"
                               },
                               {
                                   "gamma": "0.76"
                               },
                               {
                                   "visibility": "simplified"
                               },
                               {
                                   "lightness": "1"
                               }
                           ]
                       },
                       {
                           "featureType": "road.arterial",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "lightness": "0"
                               },
                               {
                                   "saturation": "-100"
                               },
                               {
                                   "gamma": "3.03"
                               }
                           ]
                       },
                       {
                           "featureType": "transit.line",
                           "elementType": "geometry",
                           "stylers": [
                               {
                                   "saturation": "-33"
                               },
                               {
                                   "lightness": "22"
                               },
                               {
                                   "gamma": "2.08"
                               },
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "water",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "color": "#9ae3e9"
                               },
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       }
                   ]
             };

             $rootScope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

             new google.maps.Marker({
                map: $rootScope.map,
                animation: google.maps.Animation.DROP,
                icon: userMarker,
                position: initialPosition
             });

             $rootScope.users = firebase.database().ref('users');

             $rootScope.truckMarkers = {};

             // At initial page load, we want to poll the database for any trucks currently signed in
             $rootScope.users.on('value', function (snapshot) {
                var trucks = snapshot.val();

                for (var key in trucks) {
                  var value = trucks[key];

                  if (value.latitude && value.longitude) {
                    var position = new google.maps.LatLng(value.latitude, value.longitude);
                    $rootScope.truckMarkers[key] = new google.maps.Marker({
                      map: $rootScope.map,
                      animation: google.maps.Animation.DROP,
                      icon: truckMarker,
                      title: value.name,
                      position: position
                    });

                    var output = 
                   '<div id="content">' +
                   '<div id="siteNotice">' + '</div>' +
                   '<h4 id="firstHeading" class="firstHeading">' + value.name + '</h4>' +
                    '</div>';

                    var infowindow = new google.maps.InfoWindow({
                      content: output,
                      pixelOffset: new google.maps.Size(0, -40),
                      position: position
                    });

                    $rootScope.truckMarkers[key].addListener('click', function() {
                      infowindow.open($rootScope.map, $rootScope.truckMarkers[key]);
                    });
                  }
                }
             });

             // Attach a listener to the child_changed event on the users database
             $rootScope.users.on('child_changed', function (data) {
                var lat = data.child('latitude').val(),
                    lon = data.child('longitude').val(),
                    truckName = data.child('name').val(),
                    position = undefined;

                if (lat.length === 0 || lon.length === 0) {
                  if (data.key in $rootScope.truckMarkers) {
                    $rootScope.truckMarkers[data.key].setMap(null);

                    delete $rootScope.truckMarkers[data.key];
                  }

                  return;
                } else {
                  position = new google.maps.LatLng(lat, lon);

                  if (data.key in $rootScope.truckMarkers) {
                    if ($rootScope.truckMarkers[data.key].getPosition().equals(position) === false) {
                      $rootScope.truckMarkers[data.key].setMap(null);

                      delete $rootScope.truckMarkers[data.key];
                    } else {
                      return;
                    }
                  }
                  
                  $rootScope.truckMarkers[data.key] = new google.maps.Marker({
                    map: $rootScope.map,
                    animation: google.maps.Animation.DROP,
                    icon: truckMarker,
                    title: truckName,
                    position: position
                  });

                    var output = 
                   '<div id="content">' +
                   '<div id="siteNotice">' + '</div>' +
                   '<h4 id="firstHeading" class="firstHeading">' + truckName + '</h4>' +
                    '</div>';

                    var infowindow = new google.maps.InfoWindow({
                      content: output,
                      pixelOffset: new google.maps.Size(0, -40),
                      position: position
                    });

                  $rootScope.truckMarkers[data.key].addListener('click', function() {
                    infowindow.open($rootScope.map, $rootScope.truckMarkers[data.key]);
                  });
                }
             });
           }, function (error) {
               $ionicPopup.alert({
                  title: 'LOCATION NOT FOUND',
                  template: "We couldn't find your location. Please relaunch the application and try again.",
                  buttons: [
                    {
                      type: 'button-assertive',
                      text: 'Relaunch',
                      onTap: function () {
                        $state.go('landing')
                      }
                    }
                  ]
               });
           });
        });

        $scope.$on('$ionicView.enter', function (event, data) {
           // If a truck is signed in we will prompt them for their location
            $rootScope.getTruckLocation = function () {
                return $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: true}).then(function (location) {
                  var lat  = location.coords.latitude,
                      lon  = location.coords.longitude;

                  firebase.database().ref('users').on('value', function (snapshot) {
                      var trucks = snapshot.val();

                      for (var key in trucks) {
                        var value = trucks[key];

                        if (value.email == firebase.auth().currentUser.email) {
                          var output = 
                         '<div id="content">' +
                         '<div id="siteNotice">' + '</div>' +
                         '<h4 id="firstHeading" class="firstHeading">' + value.name + '</h4>' +
                         '</div>';

                         var position = new google.maps.LatLng(lat, lon);

                          var infowindow = new google.maps.InfoWindow({
                            content: output,
                            pixelOffset: new google.maps.Size(0, -40),
                            position: position
                          });

                          firebase.database().ref('users/' + key).on('value', function (data) {
                            data.ref.update({latitude: lat, longitude: lon}).then(function (success) {
                              $rootScope.truckMarkers[data.key] = new google.maps.Marker({
                                map: $rootScope.map,
                                animation: google.maps.Animation.DROP,
                                icon: truckMarker,
                                title: value.name,
                                position: position
                              });
                              $rootScope.truckMarkers[data.key].addListener('click', function() {
                                infowindow.open($rootScope.map, $rootScope.truckMarkers[data.key]);
                              });
                            });
                          });

                          break;
                        }
                      }
                  });
                }, function (error) {
                   $ionicPopup.alert({
                     template: 'We could not retrieve your current location. Please relaunch the app and try again.',
                     title: 'COULD NOT RETRIEVE LOCATION',
                     buttons: [{
                       type: 'button-assertive',
                       text: '<b>Ok<b>'
                     }]
                   });

                   $state.go('landing');
                });
            };

            var user = firebase.auth().currentUser;

            if (user && !$scope.userPromptedToCheckIn) {
              $scope.userPromptedToCheckIn = true;

              $ionicPopup.alert({
                title: 'CHECK IN',
                template: "If you're currently parked and ready to post your location, click the button below. If not, just go to the Check In page when you're ready.",
                buttons: [
                  {type: 'button-assertive', text: 'Check in', onTap: $rootScope.getTruckLocation},
                  {type: 'button-assertive', text: 'Not now'}
                ]
              });
            }
        });
   })

   .controller('TrucksCtrl', function($scope, Trucks, Favorites, $ionicModal) {
         $scope.input = {};

         $scope.trucks = Trucks.all();

         $ionicModal.fromTemplateUrl('truck', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(modal) {
           $scope.modal = modal;
         });
         $scope.openModal = function() {
           $scope.modal.show();
         };
         $scope.closeModal = function() {
           $scope.modal.hide();
         };
         // Cleanup the modal when we're done with it!
         $scope.$on('$destroy', function() {
           $scope.modal.remove();
         });
         // Execute action on hide modal
         $scope.$on('modal.hidden', function() {
           // Execute action
         });
         // Execute action on remove modal
         $scope.$on('modal.removed', function() {
           // Execute action
         });

         $scope.getTruckId = function(truckData) {
            $scope.truck = truckData;

            $scope.openModal();
         };

        $scope.toggleFavorites = function (data) {
          var hasId   = Favorites.has(data.$id),
              element = document.querySelector('[data-truck-id="' + data.$id + '"] > i');

          switch (hasId) {
            case false:
              Favorites.add(data);

              element.classList.remove('ion-ios-heart-outline');
              element.classList.add('ion-ios-heart');
            break;

            default:
              Favorites.remove(data);

              element.classList.add('ion-ios-heart-outline');
              element.classList.remove('ion-ios-heart');
            break;
          }
        };

        $scope.getFavoritesIcon = function (data) {
          var has = Favorites.has(data.$id);

          if ( has === false ) {
            return 'ion-ios-heart-outline';
          } else {
            return 'ion-ios-heart';
          }
        };
   })


    .controller('FavoritesCtrl', function($scope, Favorites, Trucks, $ionicModal) {
        $scope.favorites = Favorites.get();
        $scope.trucks = Trucks.all('object');

        $ionicModal.fromTemplateUrl('truck', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(modal) {
           $scope.modal = modal;
         });

         $scope.openModal = function() {
           $scope.modal.show();
         };

         $scope.closeModal = function() {
           $scope.modal.hide();
         };

         $scope.$on('$destroy', function() {
           $scope.modal.remove();
         });
        
        // This function is responsible for opening the modal of the truck
        $scope.goToTruck = function (id) {
            $scope.truck = id;

            $scope.openModal();
        };

        $scope.removeFromList = function (data) {
          Favorites.remove(data.$id);

          var element = document.querySelector('[data-truck-id=' + data.$id + ']');

          // Code for Chrome, Safari, and Opera
          element.style.WebkitAnimationName = "fadeOutUp";

          // Standard syntax
          element.style.animationName = "fadeOutUp";

          element.style.display = 'none';

          // Refresh the scope data
          $scope.favorites = Favorites.get();
        };
    })