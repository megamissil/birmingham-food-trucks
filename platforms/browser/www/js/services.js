"use strict";

angular.module('bft.services', ['ionic', 'firebase'])

.factory("Auth", ["$firebaseAuth",
  function($firebaseAuth) {
    return $firebaseAuth();
  }
])

.factory("Trucks", ["$firebaseArray", "$firebaseObject",
  function($firebaseArray, $firebaseObject) {
    return {
      all : function(type) {
        type = typeof type === 'undefined' ? 'array' : type;

        var trucks = firebase.database().ref("users");

        switch (type) {
          case 'array':
            return $firebaseArray(trucks);
          break;

          case 'object':
            return $firebaseObject(trucks);
          break;

          default:
            return trucks;
          break;
        }
      }
    };
  }
]) 

.factory("Favorites", [ function () {
  var _get = function () {
    var favorites = localStorage.getItem('favorites');

    if ( !favorites ) {
      return [];
    } else {
      return JSON.parse(favorites);
    }
  };

  var favorites = _get();

  var _has = function (id) {
    if ( !id ) {
      return;
    }

    for (var truck in favorites) {      
      if (favorites[truck]['$id'] == id) {
        return truck;
      }
    }

    return false;
  };

  return {
    get: _get,
    has: _has,
    
    add: function (id) {
      if ( !id ) {
        return;
      }

      if ( _has(id) !== false ) {
        return;
      }

      favorites.push(id);

      localStorage.setItem('favorites', JSON.stringify(favorites));
    },

    remove: function (id) {
      if ( !id ) {
        return;
      }

      var index = _has(id);

      if ( !index ) {    
        return;
      }

      favorites.splice(index, 1);

      localStorage.setItem('favorites', JSON.stringify(favorites));
    }
  };
}]);
