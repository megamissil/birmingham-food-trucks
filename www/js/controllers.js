"use strict";

angular.module('bft.controllers', ['ionic', 'firebase', 'bft.services'])

  .controller('AppCtrl', function($scope, $cordovaInAppBrowser) {

    var deviceInformation = ionic.Platform.device();

    var isWebView = ionic.Platform.isWebView();
    var isIPad = ionic.Platform.isIPad();
    var isIOS = ionic.Platform.isIOS();
    var isAndroid = ionic.Platform.isAndroid();
    var isWindowsPhone = ionic.Platform.isWindowsPhone();

    var currentPlatform = ionic.Platform.platform();
    var currentPlatformVersion = ionic.Platform.version();

  })

     .controller("LoginCtrl", function ($rootScope, $scope, $state, $ionicLoading, $ionicPopup) {
      if ($rootScope.currentUser) {
        $state.go('app.map');
      }
      
      $scope.logIn = function (user) {
         if (user && user.email && user.password) {
            firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(
              function () {
                $rootScope.currentUser = firebase.auth().currentUser;

                $state.go('app.map');
              }, function (error) {
                $ionicLoading.hide();

                user.email = user.password = '';

                $ionicPopup.alert({
                  template: error.message,
                  title: 'LOGIN FAILED',
                  buttons: [{
                    type: 'button-assertive',
                    text: '<b>Ok</b>'
                  }]
                });

                $state.go('truck-login');
              }
            );
           } else {
             $ionicLoading.hide();
             $ionicPopup.alert({
               template: 'Please Check Credentials.',
               title: 'LOGIN FAILED',
               buttons: [{
                 type: 'button-assertive',
                 text: '<b>Ok<b>'
               }]
             });
          }
      };
   })

     .controller('MapCtrl', function($rootScope, $scope, $state, $ionicPlatform, $ionicPopup, $ionicLoading, $cordovaGeolocation, Markers) {
        // This will initially place a user on the map
        $scope.$on('$ionicView.loaded', function (event, data) {
           $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: true}).then(function (location) {
             var initialPosition = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);

             var mapOptions = {
                center: initialPosition,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [
                       {
                           "featureType": "administrative",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative",
                           "elementType": "labels.text.fill",
                           "stylers": [
                               {
                                   "color": "#e24307"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative.locality",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "administrative.locality",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "off"
                               },
                               {
                                   "color": "#f2f2ef"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.man_made",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "color": "#f2f2ef"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.natural.landcover",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "landscape.natural.terrain",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "visibility": "off"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "geometry",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               },
                               {
                                   "saturation": "-37"
                               },
                               {
                                   "lightness": "45"
                               },
                               {
                                   "gamma": "1.78"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "poi",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "poi.park",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "gamma": "1.01"
                               },
                               {
                                   "saturation": "11"
                               },
                               {
                                   "hue": "#00ff7a"
                               },
                               {
                                   "lightness": "-4"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "-100"
                               },
                               {
                                   "lightness": "25"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "-1"
                               }
                           ]
                       },
                       {
                           "featureType": "road",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "on"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "geometry.fill",
                           "stylers": [
                               {
                                   "color": "#7EBBD6"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "geometry.stroke",
                           "stylers": [
                               {
                                   "color": "#65A2BD"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "labels",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "saturation": "1"
                               },
                               {
                                   "weight": "1.00"
                               },
                               {
                                   "lightness": "1"
                               },
                               {
                                   "gamma": "1.01"
                               }
                           ]
                       },
                       {
                           "featureType": "road.highway",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "saturation": "3"
                               },
                               {
                                   "gamma": "0.76"
                               },
                               {
                                   "visibility": "simplified"
                               },
                               {
                                   "lightness": "1"
                               }
                           ]
                       },
                       {
                           "featureType": "road.arterial",
                           "elementType": "labels.icon",
                           "stylers": [
                               {
                                   "visibility": "on"
                               },
                               {
                                   "lightness": "0"
                               },
                               {
                                   "saturation": "-100"
                               },
                               {
                                   "gamma": "3.03"
                               }
                           ]
                       },
                       {
                           "featureType": "transit.line",
                           "elementType": "geometry",
                           "stylers": [
                               {
                                   "saturation": "-33"
                               },
                               {
                                   "lightness": "22"
                               },
                               {
                                   "gamma": "2.08"
                               },
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       },
                       {
                           "featureType": "water",
                           "elementType": "all",
                           "stylers": [
                               {
                                   "color": "#9ae3e9"
                               },
                               {
                                   "visibility": "simplified"
                               }
                           ]
                       }
                   ]
             };

             $rootScope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

             new google.maps.Marker({
                map:       $rootScope.map,
                animation: google.maps.Animation.DROP,
                icon:      Markers.icons.user,
                position:  initialPosition
             });

             // At initial page load, we want to poll the database for any trucks currently signed in
             $rootScope.users.once('value').then(function (snapshot) {
                var trucks = snapshot.val();

                for (var key in trucks) {
                  var value = trucks[key];

                  if (value.latitude && value.longitude) {
                    Markers.add(key, value);
                  }
                }
             });
           }, function (error) {
               $ionicPopup.alert({
                  title: 'LOCATION NOT FOUND',
                  template: "We couldn't find your location. Please relaunch the application and try again.",
                  buttons: [
                    {
                      type: 'button-assertive',
                      text: 'Relaunch',
                      onTap: function () {
                        $state.go('landing')
                      }
                    }
                  ]
               });
           });
        });

        $scope.$on('$ionicView.enter', function (event, data) {
           // If a truck is signed in we will prompt them for their location 
           google.maps.event.trigger($rootScope.map, 'resize');

            if ($rootScope.currentUser && !$rootScope.userPromptedToCheckIn) {
              $rootScope.userPromptedToCheckIn = true;

              $ionicPopup.alert({
                title: 'CHECK IN',
                template: "If you're currently parked and ready to post your location, click the button below. If not, just go to the Check In page when you're ready.",
                buttons: [
                  {
                    type: 'button-assertive',
                    text: 'Check in',
                    onTap: function () {
                      $ionicLoading.show({
                        template:   "<img src='https://storage.googleapis.com/bham-food-trucks.appspot.com/BFT-loading-pig-3.1.gif' alt='loading gif'>",
                        noBackdrop: false
                      });

                      var checkInPromise = $rootScope.checkIn();

                      checkInPromise.then(null, function (error) {
                        $ionicPopup.alert({
                          title:    'SOMETHING WENT WRONG',
                          template: 'An error occurred while trying to check in. Please relaunch the app and try again.',
                          buttons: [
                            {
                              type: 'button-assertive',
                              text: 'Ok',
                              onTap: function () {
                                $state.go('landing');
                              }
                            }
                          ]
                        });
                      });

                      checkInPromise.finally(function () {
                        $ionicLoading.hide();
                      });
                    }
                  },
                  
                  { type: 'button-assertive', text: 'Not now' }
                ]
              });
            }
        });
   })

   .controller('TrucksCtrl', function($scope, Trucks, Favorites, $ionicModal) {
         $scope.input = {};

         $scope.trucks = Trucks.all();

         $ionicModal.fromTemplateUrl('truck', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(modal) {
           $scope.modal = modal;
         });
         
         $scope.openModal = function() {
           $scope.modal.show();
         };
         
         $scope.closeModal = function() {
           $scope.modal.hide();
         };
         
         $scope.$on('$destroy', function() {
           $scope.modal.remove();
         });

         $scope.getTruckId = function(truckData) {
            $scope.truck = truckData;

            $scope.openModal();
         };

        $scope.toggleFavorites = function (data) {
          var hasId   = Favorites.has(data.$id),
              element = document.querySelector('[data-truck-id="' + data.$id + '"] > i');

          switch (hasId) {
            case false:
              Favorites.add(data);

              element.classList.remove('ion-ios-heart-outline');
              element.classList.add('ion-ios-heart');
            break;

            default:
              Favorites.remove(data);

              element.classList.add('ion-ios-heart-outline');
              element.classList.remove('ion-ios-heart');
            break;
          }
        };

        $scope.getFavoritesIcon = function (data) {
          var has = Favorites.has(data.$id);

          if ( has === false ) {
            return 'ion-ios-heart-outline';
          } else {
            return 'ion-ios-heart';
          }
        };
   })


    .controller('FavoritesCtrl', function($scope, Favorites, Trucks, $ionicModal) {
        $scope.favorites = Favorites.get();
        $scope.trucks = Trucks.all('object');

        $ionicModal.fromTemplateUrl('truck', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(modal) {
           $scope.modal = modal;
         });

         $scope.openModal = function() {
           $scope.modal.show();
         };

         $scope.closeModal = function() {
           $scope.modal.hide();
         };

         $scope.$on('$destroy', function() {
           $scope.modal.remove();
         });
        
        // This function is responsible for opening the modal of the truck
        $scope.goToTruck = function (id) {
            $scope.truck = id;

            $scope.openModal();
        };

        $scope.removeFromList = function (data) {
          Favorites.remove(data.$id);

          var element = document.querySelector('[data-truck-id=' + data.$id + ']');

          // Code for Chrome, Safari, and Opera
          element.style.WebkitAnimationName = "fadeOutUp";

          // Standard syntax
          element.style.animationName = "fadeOutUp";

          element.style.display = 'none';

          // Refresh the scope data
          $scope.favorites = Favorites.get();
        };
    })